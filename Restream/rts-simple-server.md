# RTS Simple Server
C'est un serveur supportant **RTMP**, **RTS**, **HLS** libre et [disponible sur GitHub](https://github.com/aler9/rtsp-simple-server).

Il est écrit en Go et des fichiers binaires sont disponibles pour Linux, MacOs et Windows.

## Test et utilisation en local
Avec sa configuration par défaut (celle si se trouve dans le fichier `rts-simple-server.yml`) il accepte des flux venus d'OBS par exemple où, dans les paramètres, il suffit d'indiquer 

``` rtmp://localhost ```

comme serveur de stream et

``` mystream ```

comme clé de stream.

On peut ensuite lire son stream avec, par exemple, **VLC** en ouvrant un flux réseau et en indiquant l'url

``` rtsp://localhost:8554/mystream ```