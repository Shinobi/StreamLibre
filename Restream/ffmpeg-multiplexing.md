# Multiplexer les streams avec FFMPEG
En partant d'un flux **RTSP** issu d'OBS et envoyé à un serveur RTSP/RTMP local, on récupère le flux dans _ffmpeg_ et on le duplique vers d'autres serveurs.

## Exemples de config de FFMPEG

### Vers plusieurs twitch ou twitch+youtube

_Sans modification du stream._

```
ffmpeg -i rtsp://localhost:8554/mystream -c:v copy -c:a copy -map 0 -f tee "[f=flv]rtmp://live-fra.twitch.tv/app/my-stream-key|[f=flv]rtmp://live-fra.twitch.tv/app/my-second-stream-key|[f=flv]rtmp://live-fra.twitch.tv/app/my-third-stream-key"
```

_Avec recodage du stream._

```
ffmpeg -hide_banner -i rtsp://localhost:8554/mystream
-c:v nvenc_h264 -b:v 20000k -preset slow -c:a copy -f flv  rtmp://a.rtmp.youtube.com/live2/my-youtube-stream-key
-c:v libx264 -b:v 6000k -preset veryfast -c:a copy -vf "scale=1280:720" -f flv  rtmp://live-fra.twitch.tv/app/my-stream-key
```

_Twitch + Youtube sans recodage_

```
ffmpeg -i rtsp://localhost:8554/mystream -c:v copy -c:a copy -map 0 -f tee "[f=flv]rtmp://a.rtmp.youtube.com/live2/YT-Stream-Key|[f=flv]rtmp://live-fra.twitch.tv/app/Twitch-Stream-Key"
```

Pour ce dernier exemple, j'ai testé avec un encodage NVENC(New) dans OBS à 60 fps et avec un débit initial (départ d'OBS) à 6000kbps ainsi qu'un nombre max de B-Frame à 2. Le résultat est impeccable avec le stream Twitch en avance d'environ 3-4 secondes sur YouTube.

Pour info, on peut aussi ajouter un deuxième flux Youtube si on veut utiliser le serveur de backup de Youtube en ajoutant une stream vers `rtmp://b.rtmp.youtube.com/live2?backup=1` mais je ne suis pas certain des fonctionnalités exactes de leur flux de backup (si le switch est automatique en cas de soucis, si ça reswitch automatiquement, etc). De plus ce genre de fonctionnalité me parait intéressante que si on fait passer le deuxième flux par un autre chemin réseau (une connexion au Net différente, par exemple).

### Vers trois flux locaux (pour des tests avec VLC par exemple)

```
ffmpeg -i rtsp://localhost:8554/mystream -c:v copy -c:a copy -map 0 -f tee "[f=flv]rtmp://localhost:1935/live/stream2|[f=flv]rtmp://localhost:1935/live/stream3|[f=flv]rtmp://localhost:1935/live/stream4"
```