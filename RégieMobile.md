# Régie de stream mobile

L'idée est d'avoir une régie mobile la plus simple et la moins couteuse possible pour streamer d'un peu partout, des concerts privés, des concerts de rue, des événement en tout genre.

## Les briques de base

- Un laptop pouvant encoder et diffuser en 1080p avec un flux audio en 256kbs
- OBS et tout ce qui va avec
- Une carte son pour se brancher sur une sortie ligne d'une table de mixage
- Une caméra (Sony Alpha 5100 au minimum)
- Une instance Owncast ou PeerTube pour héberger le flux
- Un modem ou modem/routeur 5G avec une antenne externe (>+6db)

### Suplément pour le son si il n'y a pas de régie son sur place

- Une table de mixage
- Un micro omnidirectionnel
