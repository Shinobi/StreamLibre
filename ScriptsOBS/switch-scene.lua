--[[
Name: Play then switch
Version: 0.1
Original Release: jan 2022
Owner: Shinobi
User Cases:
Play a media then switch to a scene.
]]
obs         = obslua
source_name = ""
scene_name  = ""
last_state  = obs.OBS_MEDIA_STATE_NONE

function script_update(settings)
    source_name = obs.obs_data_get_string(settings, "source")
    scene_name = obs.obs_data_get_string(settings, "scene")
end

function script_description()
    return "Switch to 'scene_name' after 'source_name' ends.\nSelect your source and enter the scene name to switch to."
end

function script_properties()
    props = obs.obs_properties_create()

    -- Set media source to watch
	local p = obs.obs_properties_add_list(props, "source", "Media Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
	local sources = obs.obs_enum_sources()
	if sources ~= nil then
        for _, source in ipairs(sources) do
            local name = obs.obs_source_get_name(source)
            obs.obs_property_list_add_string(p, name, name)
		end
	end
    --obs.source_list_release(sources)

    -- Set source to switch to
    local t = obs.obs_properties_add_list(props, "scene", "Scene", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
    local scenes = obs.obs_frontend_get_scene_names()
    if scenes ~= nil then
        for _, scene in ipairs(scenes) do
            obs.obs_property_list_add_string(t, scene, scene)
        end
    --    obs.bfree(scene)
    end
    -- obs.source_list_release(scenes)
    return props
end

function script_load(settings)
    script_update(settings)
end

function script_save(settings)

end

function script_tick(seconds)
    local source = obs.obs_get_source_by_name(source_name)
    if source ~= nil then
        local state = obs.obs_source_media_get_state(source)
        if last_state ~= state then
            last_state = state
            if state == obs.OBS_MEDIA_STATE_STOPPED or state == obs.OBS_MEDIA_STATE_ENDED then
                local scene_source = obs.obs_get_source_by_name(scene_name)
                if scene_source ~= nil then
                    obs.obs_frontend_set_current_scene(scene_source)
                end
            end
        end
    end
    obs.obs_source_release(source)
end