# StreamLibre

Méthodes et outils pour faire du streaming avec des outils libres voire des plateformes libres.

## Mise en place d'un serveur RTS/RTMP

- [RTS Simple Server en local](https://codeberg.org/Shinobi/StreamLibre/src/branch/main/Restream/rts-simple-server.md)

## Multiplexing avec FFMPEG

- [Exemple d'usage de ffmpeg avec le serveur RTS](https://codeberg.org/Shinobi/StreamLibre/src/branch/main/Restream/ffmpeg-multiplexing.md)

## Projet de régie de stream mobile

- [Setup mobile](RégieMobile.md)